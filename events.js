const events = require('events');
const myEvent = new events.EventEmitter();
const myEvent1 = new events.EventEmitter();

// listener
// on method

// trigger
// emit

myEvent1.on('infosys', function (data) {
  console.log('at infoys event with data >>', data)
})

setTimeout(() => {
  myEvent.emit('infosys',['typescript'])

}, 2000)