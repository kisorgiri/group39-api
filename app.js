const express = require('express');
const morgan = require('morgan');
const path = require('path');
const cors = require('cors')

// import routing level middleware
const apiRoute = require('./routes/api.route')

require('./db_init'); // part of app.js this file is loaded

// load middlewares
const isAdmin = require('./middlewares/isAdmin')

const app = express();
// app is entire express framework

// events stuff
const events = require('events');
const myEvent = new events.EventEmitter();

app.use(function (req, res, next) {
  req.myEv = myEvent;
  next();
})
myEvent.on('error', function (err, res) {
  console.log('at error event >>', err)
  res.json(err)
})

require('./socket')(app)

// express way of allocating memories
app.set('PORT', 9090)

// view engine setup////
const pug = require('pug');
app.set('view engine', pug);
app.set('views', path.join(process.cwd(), 'views'))

// view engine setup////

// load third party middleware
app.use(morgan('dev'));
app.use(cors()) // accept all request

// inbuilt middleware
// serve static files
// app.use(express.static('uploads')) 

app.use('/file', express.static(path.join(process.cwd(), 'uploads')));
// both internal and extenal usage
// parser for incoming request
// x-www-form-urlencoded
app.use(express.urlencoded({
  extended: true
}))
// it will add a body object in request which will contain data from client

// json parser
app.use(express.json())


// load routing level middleware
// mount
app.use('/api', apiRoute)

// 404 error hanlder
app.use(function (req, res, next) {
  next({
    msg: 'Not Found',
    status: 404
  })
})

// middleware function with 4 arguments is error handling middleware
// 1st argument is for error
// req,res,next is for http req, http res and next middleware function reference
// to execute error handling middleware we must call it
// to call we call next with argument
// next without argument ===> control pass garnu
// next with argument ===> error handling middleware
app.use(function (err, req, res, next) {
  console.log('at erorr handling middleware >>', err)
  // TODO set status code for error response
  res.status(err.status || 400)
  res.json({
    msg: err.msg || err,
    status: err.status || 400
  })

})

app.listen(process.env.PORT || 8080, function (err, done) {
  if (err) {
    console.log('error in listening ', err)
  } else {
    console.log('server listening at port ', app.get('PORT'));
  }
})



// middleware
// synonyms of express

// middleware is a function that has access to 
// http request object
// http response object
// next middleware function reference

// middleware function always came into action in between req-res cycle

// NOTE:- The order of middleware function matters

// syntax
//  function(req,res,next){
  //  req or 1st argumnet is http request object
  // res or 2nd argument is http response object
  // next or 3rd argument is next middleware function reference
//  }

// app.use(), http verb, all
// use method is used to configure middleware function

// types of middleware
// 1. application level middleware
// 2. routing level middleware
// 3. third party middleware
// 4. inbuilt middleware
// 5. erorr handling middleware


// application level middleware
// those middleware which has direct scope of http req object http response object and next middleware function reference are application level middleware


// templating engine
// pug,jade,hbs,ejs

//note we have  to parse incoming data in our application to view data in controller
// parser should be for each content type
// server must parse incoming data
// server must use parser according to content type