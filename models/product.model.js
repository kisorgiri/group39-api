const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ReviewSchema = new Schema({
  point: {
    type: Number,
    min: 1,
    max: 5,
  },
  message: {
    type: String,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
}, {
  timestamps: true
})

const ProductSchema = new Schema({
  name: String,
  description: String,
  category: {
    type: String,
    required: true
  },
  modelNo: String,
  price: Number,
  weight: String,
  color: String,
  quantity: Number,
  manuDate: Date,
  expiryDate: Date,
  soldDate: Date,
  purchasedDate: Date,
  returnedDate:Date,
  status: {
    type: String,
    enum: ['available', 'out of stock', 'booked','sold'],
    default: 'available'
  },
  batchNo: String,
  reviews: [ReviewSchema],
  vendor: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  images: [String],
  brand: String,
  warrentyStatus: Boolean,
  warrentyPeriod: String,
  offers: [String],
  discount: {
    discountedItem: Boolean,
    discountType: {
      type: String,
      enum: ['percentage', 'quantity', 'value']
    },
    discountValue: String
  },
  isReturnEligible: Boolean,
  tags: [String]
}, {
  timestamps: true
})

module.exports = mongoose.model('product', ProductSchema)


// identify entities and attributes for
// Task management system 
// social media handle
// pathao