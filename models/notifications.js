const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NotificationSchema = new Schema({
  msg: String,
  seen: Boolean,
  delivered: Boolean,
  type: String,
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
}, {
  timestamps: true
})

module.exports = mongoose('notification', NotificationSchema);

// prepare controller for
// Fetch all notification (loggedIn user's id and user value must match)
// search or appropriate name ==>where seen , delivered ,user ... filter can be applied

// markAsRead

// MarkAllAsRead

// 