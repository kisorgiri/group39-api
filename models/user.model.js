const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  // db modelling
  name: {
    type: String,
    trim: true,
    lowercase: true
  },
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  email: {
    type: String,
    sparse: true,
    unique: true
  },
  phoneNumber: {
    type: Number
  },
  address: {
    temporaryAddress: [String],
    permanentAddress: String
  },
  dob: {
    type: Date
  },
  gender: {
    type: String,
    enum: ['male', 'female', 'others']
  },
  nationality: {
    type: String,
    default: 'nepali'
  },
  role: {
    type: Number, //1 for admin , 2 for normal user
    default: 2
  },
  status: {
    type: String,
    default: 'active'
  },
  image: String,
  isMarried: Boolean,
  passwordResetExpiry: Date
}, {
  timestamps: true
})

const UserModel = mongoose.model('user', UserSchema);

module.exports = UserModel;

// try 
// review modelling
// notification modelling
