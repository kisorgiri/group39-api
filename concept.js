// Node JS  ==> run time env

// npm 
// npx 
// pacakge management tool

// command
// npm init ==> project initilization

// pacakge.json ==> project introductory file
// package-lock.json || yarn.lock ==> lock file to lock exact version of pacakges during intial installation
// node_modules ==> folder to keep installed pacakges

// (note all dependent packages are installed inside node modules)
// npmjs.com ==> global repository (npm registry) where JS pacakges are hosted


// command
// npm install <pacakge_name>

// 

// file - file communication

// one file must export

// one file must import

// export syntax
// es5
// module.exports = any_value [function,array,object,string,number,boolean]
// es6
// export const a, export class b

// export default <any value>


// import syntax
// es5
// const any_name = require(path of file)

// note:- we dont have to give path to import modules that are either nodejs's internal or installed from npmjs.com

// es6
// import {name1,name2} from 'path to source file';
// import any_name from 'path to source file'


// web application is out main objective


// web architecture
// MVC architecture

// Models ==> database
// Views ===> user interaction
// Controllers ==> business logic

// data layer
// presentation layer
// controller layer


// 2- tier architecture
// client programme (programme that initiate request)
// server programme(response dine programme )


// future 
// client programme , react > web application, reactnative > mobile appliction ,electron > desktop application


// server programme
// nodejs 


// protocol
// set of rules to commmunicate between two programme

// smtp
// ftp
// mqtt
// http


// http server

// http 
// http verb(method)
// get,put post, delete
// http headers
// http request (object)
// http response(object)

// http status code
// 100 range information
// 200 success range
// 300 ridirection
// 400 application erorr
// 500 server error

// Authentication and Authorization

// Authentication
// chinne process
// login with username and password

// session and cookies
// 

// token based authentication


// REST API

// API (Application Programming Interface)
// http methods, http URLS
// END POINT ==> combination of http method and url
// these endpoints are API

// REST architecture
// (Representational State Transfer)
// an API is Restful whenever these points are valid

// 1. stateless==> server will not save any information from client
// each http request is independent

// 2. correct use of http verb
// GET ==> data fetch
// PUT/PATCH ==> data update
// POST ==> data send
// DELETE ==> data remove

// 3. data format must be in either JSON or XML

// 4. caching db call, [Only in GET method ]

// REST API

// remaining topics to be covered
// Email ==> forgot password
// web socket ==> 
// CORS,statusCode
// aggregation 