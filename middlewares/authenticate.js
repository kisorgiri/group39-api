const config = require('./../configs');
const JWT = require('jsonwebtoken');
const UserModel = require('../models/user.model');

module.exports = function (req, res, next) {
  // token verifcation
  let token;
  if (req.headers['authorization'])
    token = req.headers['authorization'];

  if (req.headers['x-access-token'])
    token = req.headers['x-access-token']

  if (req.query.token)
    token = req.query.token;

  if (!token) {
    return next({
      msg: 'Authentication Failed, Token Not provided',
      status: 403
    })
  }

  // extract exact token
  token = token.split(' ')[1];

  // token found now validate
  JWT.verify(token, config.JWT_SECRET, function (err, done) {
    if (err) {
      return next(err);
    }
    UserModel.findById(done._id, function (err, user) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return next({
          msg: 'User Removed from system',
          status: 404
        })
      }
      req.user = user;
      next();
    })
  })
}

// prepare a middleware to authorize request 
// logic check if role is 1 if yes proceed else block request