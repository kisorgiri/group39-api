const socket = require('socket.io');
const config = require('./configs')

// const io = socket
var users = [];

module.exports = function (app) {
  const io = socket(app.listen(config.SOCKET_PORT), {
    cors: {
      allow: '/*'
    }
  });
  io.on('connection', function (client) {
    var id = client.id;
    console.log('client connected to socket server');
    client.on('new-user', function (name) {
      console.log('new user on messaging channel', name)
      users.push({
        id,
        name
      })
      client.broadcast.emit('users', users)
      client.emit('users', users)
    })

    client.on('new-message', function (data) {
      console.log('at new-message eventr in server >>', data)
      // client.emit('reply-message', data)
      // *******stable behavior**********
      // emit will only broadcast to requesting client
      // to bradcast event to all other connected client except requesting one
      // broadcast.emit()
      // *******stable behavior**********


      //  unstable behaviour verison 4.4.0 ==> brodcast.emit will trigger event to every client including to sender
      // client.broadcast.emit('reply-message', data) 
      // broadcast.to(clientid).emit('event',data) // for individual client
      client.broadcast.to(data.receiverId).emit('reply-message-to', data)
      client.emit('reply-message', data)
    })

    client.on('disconnect', function () {
      users.forEach(function (user, index) {
        if (user.id === id) {
          users.splice(index, 1);
        }
      })
      client.broadcast.emit('users', users)
    })
  })

}