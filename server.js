const http = require('http');
const PORT = 9090;
const fileOperation = require('./file')

var server = http.createServer(function (req, res) {
  // req or 1st argument is http request object
  // res or 2nd argument is http response object
  // this callback will execute for every client request

  console.log('client connected to server');
  console.log('req.url >>>', req.url);
  console.log('req.method >>>', req.method)
  const url = req.url.split('/');
  const action = url[1];
  const fileName = url[2];
  const content = url[3];
  console.log('url is >>', url)
  if (action === 'write') {
    fileOperation.write(fileName, content)
      .then(function (data) {
        res.end('success', data)
      })
      .catch(function (err) {
        res.end('failure ', err)
      })
  }
  else if (action === 'read') {

  }
  else if (req.url === '/rename') {

  } else {
    res.end('no any action perform')
  }

  // Regardless of http method and http url this callback will came into action

  // req-res cycle must be completed
  // else client will wait for response
})

// client needs to call the endpoint to execute above listeners
// endpoint ==> combination of http verb and URL

// /comment POST
// GET /user


server.listen(PORT, function (err, done) {
  if (err) {
    console.log('err in listening >>', err)
  } else {
    console.log('server listening at port ' + PORT);
    console.log('press CTRL + C to exit')
  }
})


// task
// 1. create server perform req-res cycle

// 2nd task
// perform file operation inside server according to their req url
// eg is above for write
// perform for rename,read, remove

// challanges
// take values from url 
// eg if req.url ===> localhost:4000/write/kishor.txt/i am kishor

// filename must be kishor.txt
// contentn must be i am kishor

// similar for read,rename and remove