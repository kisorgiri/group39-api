const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model')
const MapUserReq = require('./../helpers/mapUserReq')
const uploader = require('./../middlewares/uploader')
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const config = require('./../configs');
const nodemailer = require('nodemailer');

const sender = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: 'broadwaytest44@gmail.com',
    pass: 'Broadwaytest44!'
  }
})

function prepareMail(data) {
  return {
    from: 'Group39 Web Store', // sender address
    to: data.email + ',ishworineupane135@gmail.com', // list of receivers
    subject: "Forgot Password ✔", // Subject line
    text: "Forgot Password", // plain text body
    html: `
    <div>
    <p>Hi <strong> ${data.name} </strong>,</p>
    <p>We noticed that you are having trouble logging into our system please click the link below to reset your password.</p>
    <p><a href="${data.link}">Click here to reset password</a></p>
    <p>Regards</p>
    <p>Group39 Support Team</p>
    <p>2021</p>
    </div>`, // html body
  }
}

function createToken(user) {
  return jwt.sign({
    _id: user._id,
    username: user.username,
    role: user.role
  }, config.JWT_SECRET);
}

router.get('/', function (req, res, next) {
  require('fs').readFile('sdlkfjasldkfj', function (err, done) {
    if (err) {
      req.myEv.emit('error', err, res)
    }
  })
})

router.post('/login', function (req, res, next) {

  UserModel
    .findOne({
      $or: [{
        username: req.body.username
      }, {
        email: req.body.username
      }]
    })
    .then(function (user) {
      // if user 
      // password verification
      // token generation
      // send response
      if (!user) {
        return next({
          msg: 'Invalid Username',
          status: 400
        })
      }
      let isMatched = passwordHash.verify(req.body.password, user.password);
      if (!isMatched) {
        return next({
          msg: 'Invalid Password',
          status: 400
        })
      }
      let token = createToken(user);
      res.json({
        user: user,
        token: token
      })

    })
    .catch(function (err) {
      next(err)
    })


})

router.post('/register', uploader.single('image'), function (req, res, next) {
  console.log('req.file >>', req.file);
  console.log('req.body >>', req.body)
  if (req.fileTypeErr) {
    return next({
      msg: 'Invalid File Type',
      status: 400
    })
  }

  if (req.file) {
    var mime_type = req.file.mimetype.split('/')[0]
    if (mime_type !== 'image') {
      fs.unlink(path.join(process.cwd(), 'uploads/images/' + req.file.filename), function (err, done) {
        if (!err) {
          console.log('file removed')
        }
      })
      return next({
        msg: "Invalid File Format",
        status: 400
      })
    }

    req.body.image = req.file.filename;
  }
  // db operation
  let newUser = new UserModel({});
  // newUser ==> mongoose's object

  const newMappedUser = MapUserReq(req.body, newUser)
  newMappedUser.password = passwordHash.generate(req.body.password)
  newMappedUser.save(function (err, done) {
    if (err) {
      return next(err);
    }
    res.json(done)
  })

})

router.post('/forgot-password', function (req, res, next) {
  UserModel.findOne({
    email: req.body.email
  }, function (err, user) {
    if (err) {
      return next(err);
    }
    if (!user) {
      next({
        msg: 'No any account associated with provided email',
        status: 404
      })
    }
    // user found now send email
    const emailData = {
      name: user.username,
      email: user.email,
      link: `${req.headers.origin}/reset_password/${user._id}`
    }
    const passwordResetExpiry = new Date().getTime() + (1000 * 60 * 60 * 24);

    const emailContent = prepareMail(emailData);

    user.passwordResetExpiry = new Date(passwordResetExpiry);
    user.save(function (err, done) {
      if (err) {
        return next(err)
      }
      sender.sendMail(emailContent, function (err, done) {
        if (err) {
          return next(err)
        }
        res.json(done)
      })
    })

  })
})

router.post('/reset-password/:userId', function (req, res, next) {
  UserModel.findOne({
    _id: req.params.userId,
    passwordResetExpiry: {
      $gte: Date.now()
    }
  })
    .then(function (user) {
      if (!user) {
        return next({
          msg: 'Password Reset Link Invalid or Expired',
          status: 400
        })
      }
      // const expiryTime = new Date(user.passwordResetExpiry).getTime();
      // console.log('expiry time >>', expiryTime)
      // const now = new Date().getTime();
      // if (expiryTime < now) {
      //   return next({
      //     msg: "Password reset link expired",
      //     status: 400
      //   })
      // }
      user.passwordResetExpiry = null;
      user.password = passwordHash.generate(req.body.password)
      user.save(function (err, done) {
        if (err) {
          return next(err);
        }
        res.json(done)
      })
    })
    .catch(function (err) {
      next(err)
    })

})

module.exports = router;
