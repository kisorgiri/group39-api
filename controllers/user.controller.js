const router = require('express').Router();
const UserModel = require('./../models/user.model');
const MapUserReq = require('./../helpers/mapUserReq');
const uploader = require('./../middlewares/uploader')

router.route('/')
  .get(function (req, res, next) {
    // get all user
    UserModel
      .find({})
      .sort({
        _id: -1
      })
      // .limit(2)
      // .skip(1)
      .exec(function (err, users) {
        if (err) {
          return next(err);
        }
        res.json(users)
      })

  })

router.route('/search')
  .get(function (req, res, next) {
    res.send('from search endpoint')
  })
  .post(function (req, res, next) {
    res.send('from user search')
  });

router.route('/:id')
  .get(function (req, res, next) {
    // get by id
    UserModel
      .findById(req.params.id)
      .then(function (user) {
        res.json(user)
      })
      .catch(function (err) {
        next(err);
      })

  })
  .put(uploader.single('image'), function (req, res, next) {
    // update
    if (req.fileTypeErr) {
      return next({
        msg: 'Invalid file format',
        status: 400
      })
    }
    if (req.file) {
      req.body.image = req.file.filename
    }
    UserModel.findById(req.params.id, function (err, user) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return next({
          msg: 'User not Found',
          status: 404
        })
      }
      // user is also mongoose object

      const updatedMappedUser = MapUserReq(req.body, user)
      // user is now updated object
      updatedMappedUser.save(function (err, done) {
        if (err) {
          return next(err);
        }
        res.json(done);
        // TODO remove existing old image if req.file
      })

    })

  })
  .delete(function (req, res, next) {
    UserModel.findById(req.params.id, function (err, user) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return next({
          msg: 'User not found',
          status: 404
        })
      }
      user.remove(function (err, done) {
        if (err) {
          return next(err);

        }
        res.json(done)
      })
    })
  });



module.exports = router;
