const ProductModel = require('./../models/product.model');
const path = require('path');
const fs = require('fs');

/**
 * 
 * @param {*} productData 
 * @param {*} product 
 */
function mapProductReq(productData, product) {
  if (productData.name)
    product.name = productData.name;
  if (productData.description)
    product.description = productData.description;
  if (productData.category)
    product.category = productData.category;
  if (productData.modelNo)
    product.modelNo = productData.modelNo;
  if (productData.price)
    product.price = productData.price;
  if (productData.weight)
    product.weight = productData.weight;
  if (productData.color)
    product.color = productData.color;
  if (productData.quantity)
    product.quantity = productData.quantity;
  if (productData.manuDate)
    product.manuDate = productData.manuDate;
  if (productData.expiryDate)
    product.expiryDate = productData.expiryDate;
  if (productData.soldDate)
    product.soldDate = productData.soldDate;
  if (productData.purchasedDate)
    product.purchasedDate = productData.purchasedDate;
  if (productData.returnedDate)
    product.returnedDate = productData.returnedDate;
  if (productData.status)
    product.status = productData.status;
  if (productData.batchNo)
    product.batchNo = productData.batchNo;
  if (productData.vendor)
    product.vendor = productData.vendor;
  if (productData.images)
    product.images = productData.images;
  if (productData.brand)
    product.brand = productData.brand;
  if (productData.warrentyStatus)
    product.warrentyStatus = productData.warrentyStatus;
  if (productData.warrentyPeriod)
    product.warrentyPeriod = productData.warrentyPeriod;
  if (productData.offers)
    product.offers = typeof (productData.offers) === 'string' ? productData.offers.split(',') : productData.offers
  if (productData.tags)
    product.tags = typeof (productData.tags) === 'string' ? productData.tags.split(',') : productData.tags
  if (productData.isReturnEligible)
    product.isReturnEligible = productData.isReturnEligible;
  if (!product.discount)
    product.discount = {};
  if (productData.discountedItem)
    product.discount.discountedItem = productData.discountedItem;
  if (productData.discountType)
    product.discount.discountType = productData.discountType;
  if (productData.discountValue)
    product.discount.discountValue = productData.discountValue;
}

/**
 * find product data
 * @param {object} req 
 * @param {object} res 
 * @param {Function} next
 */
function find(req, res, next) {
  let condition = {};
  if (req.user.role !== 1) {
    condition.vendor = req.user._id
  }
  customFind(condition)
    .then(function (products) {
      res.json(products)
    })
    .catch(function (err) {
      next(err)
    })
}

function findById(req, res, next) {
  let condition = { _id: req.params.id };
  customFind(condition)
    .then(function (products) {
      res.json(products[0])
    })
    .catch(function (err) {
      next(err)
    })
}

function search(req, res, next) {
  let searchCondition = {};
  // todo prepare search searchCondition
  const data = req.method === 'GET' ? req.query : req.body;
  if (data.category)
    searchCondition.category = data.category;
  if (data.name)
    searchCondition.name = data.name;
  if (data.color)
    searchCondition.color = data.color;
  if (data.brand)
    searchCondition.brand = data.brand;
  if (data.minPrice)
    searchCondition.price = {
      $gte: data.minPrice
    }
  if (data.maxPrice)
    searchCondition.price = {
      $lte: data.maxPrice
    }
  if (data.minPrice && data.maxPrice)
    searchCondition.price = {
      $lte: data.maxPrice,
      $gte: data.minPrice
    }

  if (data.fromDate && data.toDate) {
    const fromDate = new Date(data.fromDate).setHours(0, 0, 0, 0);
    const toDate = new Date(data.toDate).setHours(23, 59, 59, 999);
    searchCondition.createdAt = {
      $gte: new Date(fromDate),
      $lte: new Date(toDate)
    }
  }
  // $all ==> provided items all shhould be matched
  // $in => any value from provdided array can match
  if (data.tags)
    searchCondition.tags = {
      $in: data.tags.split(',')
    }

  customFind(searchCondition)
    .then(function (products) {
      res.json(products)
    })
    .catch(function (err) {
      next(err)
    })
}

function insert(req, res, next) {
  const newProduct = new ProductModel({});
  const data = req.body;
  if (req.fileTypeErr) {
    return next({
      msg: 'Invalid file format',
      status: 400
    })
  }
  // map incoming request
  // prepare data 
  // 1. add vendor in req data,
  // 2. add images 
  data.vendor = req.user._id;
  if (req.files && req.files.length) {

    data.images = req.files.map(function (file) {
      return file.filename;
    })
  }

  mapProductReq(data, newProduct)
  newProduct
    .save()
    .then(function (saved) {
      res.json(saved);
    })
    .catch(function (err) {
      next(err);
    })

}

function update(req, res, next) {
  const data = req.body;
  // remove vendor 
  delete data.vendor;
  // images will be in string and it will not be used anywhere when updating
  delete data.images;
  let newImages = [];
  if (req.files && req.files.length) {
    newImages = req.files.map(file => file.filename);
  }

  ProductModel.findById(req.params.id, function (err, product) {
    if (err) {
      return next(err);
    }
    if (!product) {
      return next({
        msg: 'Product Not Found',
        status: 404
      })
    }
    // product found now update
    mapProductReq(data, product)
    var existingImages = product.images;

    // remove from product's images
    if (req.body.filesToRemove) {
      // intoArray
      const filesToRemove = req.body.filesToRemove.split(',');
      [...existingImages].forEach(function (img, index) {
        if (filesToRemove.includes(img)) {
          existingImages.splice(existingImages.indexOf(img), 1)
          removeImage(img)
        }
      })
    }

    // add new images in images array
    product.images = [
      ...existingImages,
      ...newImages
    ];
    product.save(function (err, updated) {
      if (err) {
        return next(err);
      }
      res.json(updated)
    })
  })
}

function remove(req, res, next) {
  ProductModel.findByIdAndRemove(req.params.id, function (err, removed) {
    if (err) {
      return next(err);
    }
    if (!removed) {
      return next({
        msg: "Product not found",
        status: 404
      })
    }
    if (removed.images && removed.images.length > 0) {
      removed.images.forEach(function (item, index) {
        removeImage(item)
      })
    }
    res.json(removed)
  })

}

function addReview(req, res, next) {
  const productId = req.params.productId;
  const reviewData = req.body;

  if (!(reviewData.point && reviewData.message)) {
    return next({
      msg: "Missing review point or message",
      status: 400
    })
  }

  const newReview = {
    user: req.user._id
  };
  if (reviewData.point)
    newReview.point = reviewData.point;
  if (reviewData.message)
    newReview.message = reviewData.message;


  ProductModel.findById(productId, function (err, product) {
    if (err) {
      return next(err);
    }
    if (!product) {
      return next({
        msg: 'Product not found',
        status: 404
      })
    }
    product.reviews.push(newReview);
    product.save(function (err, saved) {
      if (err) {
        return next(err);
      }
      res.json(saved)
    })
  })
}

function customFind(condition) {
  return ProductModel
    .find(condition)
    .sort({
      _id: -1
    })
    .populate('vendor', {
      username: 1
    })
    .populate('reviews.user', {
      email: 1
    })
    .exec();
}

function removeImage(img) {
  fs.unlink(path.join(process.cwd(), 'uploads/images/' + img), function (err, done) {
    if (err) {
      console.log('item removed failed', err)
    } else {
      console.log('removed')
    }
  })
}

module.exports = {
  find,
  findById,
  search,
  insert,
  update,
  remove,
  addReview
}



