module.exports = function (userData, user) {
  if (userData.username)
    user.username = userData.username;
  if (userData.name)
    user.name = userData.name;
  if (userData.password)
    user.password = userData.password;
  if (userData.phoneNumber)
    user.phoneNumber = userData.phoneNumber;
  if (userData.email)
    user.email = userData.email;
  if (userData.dob)
    user.dob = userData.dob;
  if (userData.gender)
    user.gender = userData.gender;
  if (userData.status)
    user.status = userData.status;
  if (userData.role)
    user.role = userData.role;

  if (userData.isMarried == true || userData.isMarried == false)
    user.isMarried = userData.isMarried

  if (!user.address)
    user.address = {};
  if (userData.temporaryAddress)
    user.address.temporaryAddress = userData.temporaryAddress.split(',');
  if (userData.permanentAddress)
    user.address.permanentAddress = userData.permanentAddress;
  if (userData.image)
    user.image = userData.image

  return user;
}