
const router = require('express').Router();
const productRoute = require('./product.route');
const authRoute = require('./../controllers/auth.controller');
const userRoute = require('./../controllers/user.controller');

// middlewares 
const authenticate = require('./../middlewares/authenticate');

router.get('/health_check', function (req, res, next) {
  res.json({
    msg: 'API is live'
  })
})

router.use('/auth', authRoute);
router.use('/user', authenticate, userRoute);
router.use('/product', productRoute);


module.exports = router;