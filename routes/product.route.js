const router = require('express').Router();
const ProductCtrl = require('./../controllers/product.controller')
const uploader = require('./../middlewares/uploader');
const authenticate = require('./../middlewares/authenticate');

router.route('/')
  .get(authenticate, ProductCtrl.find)
  .post(authenticate, uploader.array('images'), ProductCtrl.insert);

router.route('/search')
  .get(ProductCtrl.search)
  .post(ProductCtrl.search);

router.route('/add_review/:productId')
  .post(authenticate, ProductCtrl.addReview)

router.route('/:id')
  .get(authenticate, ProductCtrl.findById)
  .put(authenticate, uploader.array('images'), ProductCtrl.update)
  .delete(authenticate, ProductCtrl.remove);

module.exports = router;