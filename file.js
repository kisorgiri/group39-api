const fs = require('fs');
// write
// fs.writeFile('./files/abcd.txt', 'hello and welcome', function (err, done) {
//   if (err) {
//     console.log('error in writing >>', err)
//   } else {
//     console.log('success in writing >>', done)
//   }
// })

// read
// fs.readFile('./files/broadway.js','UTF-8', function (err, done) {
//   if (err) {
//     console.log('error is >>>', err)
//   } else {
//     console.log('success in read >>', done)
//   }
// })


// rename 
// fs.rename()


// remove
// fs.unlink

// create your own function(task);

// handle result of your own task

// separate task part and execution part in different file
// perform file-file communication


// function myWrite(fileName, content, cb) {
//   fs.writeFile('./files/' + fileName, content, function (err, done) {
//     if (err) {
//       cb(err);
//     } else {
//       cb(null, 'success')
//     }
//   })
// }

// myWrite('test.js', 'testing some server side stuff', function (err, done) {
//   if (err) {
//     console.log('err of my write >>', err)
//   } else {
//     console.log('success of my write >>', done)
//   }
// })

function myWrite(fileName, content) {
  return new Promise(function (resolve, reject) {
    fs.writeFile('./files/' + fileName, content, function (err, done) {
      if (err) {
        return reject(err)
      }
      resolve(done);
    })
  })
}

// myWrite('kishor.js', 'i am kishor')
//   .then(function (data) {
//     console.log('succes in promise >>', data)
//   })
//   .catch(function (err) {
//     console.log('failure in promsie >>>', err)
//   })

module.exports = {
  write: myWrite
}