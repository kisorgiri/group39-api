
// database==> container
// database ==> data manipulation ==> database management system

// 1.Relational DBMS
// 2. Distributed DBMS

// 1. Relational DBMS
// a. Table based design (entities)
// Student , College
// b. each record inside table is row/tuple
// c.relation between table exists
// (foreign key , primary_key)
// d. Schema based design
// e. non scalable
// f. SQL database 
// g. mysql, sql-lite, postgres ....


// Distributed DBMS
// a. Collection based design 
// Books, Reviw, Users 
// b. each record inside collection are document
// document based database
// document ==> document is any valid JSON 
// c. Relation doesnot exists
// d. Schema less design
// e. highly scalable
// f.NO SQL database not only sql
// g. mongodb,dynamodb,couchdb, cosmosdb, redis



// task install mongodb
// from terminal in any location you should be able to run mongo command
//  > arrow head interface should be visible

// hint
// programme files
// mongodb/version/bin/===>copy this path

// environment variables >>> system variables ===> path ===> edit or add new path 


// client -server communication
// server programme must be installed and run in the machine

// http://host:port
// mongodb://host:27017

// CRUD operation


// communication with mongod shell

// mongo command 
// > interface is there to run shell command

// mongo sheel command

// show dbs ===> list all the available databases
// use <db_name> if db_name exists select existing database else create new database and select it
// db ==> prints selected db

// show collections ==> list all the available databse from selected db

// C Create

// db.<collection_name>.insert({valid json})
// db.<collection_name>.insertMany({validJSON})

// Read
// db.<collection_name>.find({query_builder}) // shows the result of find
// db.<collection_name>.find({query_builder}).pretty(): //  format the result
// db.<collection_name.find({condition}).count(); // returns count of document

// projection
// db.<collection_name>.find({condition},{projection})

// sort  ==> query.sort({field :-1 for descending})
// limit==> .limit(limit count)
// skip==> .skip(skip_count)

// range search
// $gt ==> greater then
// $gte greater then equals
// $lt less then
// $lte less then equals

// given tags ==> ['i7','hp']
// $all ==> provided given tags all element must be matched  AND
// $in ==> OR ==> provided given tags either of provided value can be matched

// aggregation==> TODO


// update
// db.<collection_name>.update({},{},{})
// 1st object ==> query builder
// 2nd object ==> it must have $set as an key and object as an value 
// object is payload to be updated
// 3rd object is options [options are always optional]
// db.<collection_name>.update({_id:ObjectId('id')},{$set:{prop:value}},{mutli:true,upsert:true})


// Delete 
// db.<collection_name>.remove({condition})

// NOTE :- don't leave the condition empty



// ODM/ORM ==> tool to communicate with database

// Mongoose ===> ODM

// advantages of using ODM
// . Schema based solution
// . indexing is lot more easire
// . custom methods of mongoose
// . middleware
// . data type

// db connection is always open

// database backup and restore
// 1. bson structure
// 2.csv/json structure

// mongodump, mongorestore,
// mongoexport, mongoimport

// 1.bson structure
// mongodump, mongorestore

// backup
// command ==> mongodump
// mongodump ==> it will create a default dump folder to backup all the available database
// mongodump --db <db_name> --> it will create backup in dump folder for selected database
// mongoddump --db <db_name> --out <path_to_desired_destination>

// restore
// command ==> mongorestore
// mongorestore ==> it will look after dump folder and tries to import all databases
// mongorestore <path_to_backup_source>  it will restore from given source
// mongorestore --drop  it will drop existing collections

// json and csv
// json
// backup
// command == mongoexport

// mongoexport --db <db_name> --collection <collection_name> --out <path_to_destination_with.json extension>

// import from json
// mongoimport --db [new db or existing db] --collection [new collection or existingcollection ] <path_to_source>

// csv
// backup
// mongoexport --db <db_name> --collection <collection_name> --type=csv --fileds 'comma separated;' field to be exported' --out <path_to_destination_with.csv extension>
// mongoexport --db <db_name> --collection <collection_name> --type=csv  --query='{"property_name":"value"}'--fileds 'comma separated field to be exported' --out <path_to_destination_with.csv extension>
// mongoexport -d <db_name> -c <collection_name> --type=csv  -q='{"property_name":"value"}'-f 'comma separated field to be exported' -0 <path_to_destination_with.csv extension>


// import
// mongoimport  --db <new or existing db> --collection <new or existing collection> --type=csv <path_to_source>  --headerline

